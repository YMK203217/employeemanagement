package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.EmployeeDao;
import com.example.demo.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	private EmployeeDao theEmployeeDao;
	
	@Autowired
	public EmployeeServiceImpl(@Qualifier("employeeJPADaoImpl") EmployeeDao employeeDao) {
		theEmployeeDao = employeeDao;
	}
	
	@Override
	@Transactional
	public List<Employee> findAll() {
		return theEmployeeDao.findAll();
	}

	@Override
	@Transactional
	public void save(Employee theEmployee) {
		theEmployeeDao.save(theEmployee);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		theEmployeeDao.deleteById(theId);
	}

	@Override
	@Transactional
	public Employee findById(int theId) {
		return theEmployeeDao.findById(theId);
	}

}
