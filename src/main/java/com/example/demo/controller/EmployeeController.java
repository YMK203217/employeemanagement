package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	private EmployeeService employeeServices;
	
	@Autowired
	public EmployeeController(EmployeeService theEmployeeService) {
		employeeServices = theEmployeeService;
	}
	
	@GetMapping("/ok")
	public List<Employee> findAll(){
	return employeeServices.findAll();
	}
	
	@GetMapping("/employee/{employeeId}")
	public Employee getEmployeeInfo(@PathVariable int employeeId) {
		
		Employee theemployee = employeeServices.findById(employeeId);
		
		if(theemployee == null) {
			throw new RuntimeException("Employee Id is not found : "+employeeId);
		}
		return theemployee;
	}
	
	@PostMapping("/employee")
	public Employee addEmployeeInfo(@RequestBody Employee theEmployee) {
		theEmployee.setId(0);
		employeeServices.save(theEmployee);
		return theEmployee;
	}
	
	@PutMapping("/employee")
	public Employee updateEmployeeInfo(@RequestBody Employee theEmployee) {
		employeeServices.save(theEmployee);
		return theEmployee;
	}
	
	@DeleteMapping("/employee/{employeeId}")
	public String deleteEmployeeInfo(@PathVariable int employeeId) {
		Employee tempEmployee = employeeServices.findById(employeeId);
		
		if(tempEmployee == null) {
			throw new RuntimeException("Employee Id is not found : "+employeeId);
		}
		employeeServices.deleteById(employeeId);
		
		return "Deleted Id===>>"+employeeId;
		
	}
	
}
