package com.example.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Employee;

@Repository
public class EmployeeJPADaoImpl implements EmployeeDao{
	
private EntityManager entityManager;
	
	@Autowired
	public EmployeeJPADaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
		}

	@Override
	public List<Employee> findAll() {
		Query theQuery = entityManager.createQuery("from Employee");
		List<Employee> theEmployee = theQuery.getResultList();
		return theEmployee;
	}

	@Override
	public void save(Employee theEmployee) {
		Employee theemployee = entityManager.merge(theEmployee);
		theEmployee.setId(theemployee.getId());
	}

	@Override
	public void deleteById(int theId){
		Query deleteQuery = entityManager.createQuery("delete from Employee where id=:employeeId");
		deleteQuery.setParameter("employeeId", theId);
		deleteQuery.executeUpdate();
	}

	@Override
	public Employee findById(int theId) {
		Employee dbEmployee = entityManager.find(Employee.class, theId);
		return dbEmployee;
	}

}
