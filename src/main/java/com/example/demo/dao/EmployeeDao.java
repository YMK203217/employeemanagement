package com.example.demo.dao;

import java.util.List;

import com.example.demo.entity.Employee;

public interface EmployeeDao {
	
	public List<Employee> findAll();
	
	public void save(Employee theEmployee);
	
	public void deleteById(int theId);
	
	public Employee findById(int theId);
	

}
